<?php

namespace Validator\Rules;

class InArray extends AbstractRule
{
    /**
     * Is case sensitive
     * @var bool
     */
    protected $caseSensitive = false;

    /** 
     * Array of values
     * @var array 
     */
    protected $values = [];

    /**
     * Initialise values
     * @param array $values
     * @param bool $caseSensitive
     */
    public function __construct(array $values, bool $caseSensitive = false)
    {
        $this->values = $values;
        $this->caseSensitive = $caseSensitive;
    }

    /**
     * @inheritDoc
     */
    public function message($attribute): string
    {
        return "Attribute {$attribute} is invalid.";
    }

    /**
     * @inheritDoc
     */
    public function validate($value): bool
    {
        return in_array(
            $this->caseSensitive ? strtolower($value) : $value,
            $this->caseSensitive ? array_map('strtolower', $this->values) : $this->values,
            true
        );
    }
}