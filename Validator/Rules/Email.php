<?php

namespace Validator\Rules;

class Email extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public function validate($value): bool
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }
}