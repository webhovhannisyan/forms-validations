<?php

namespace Validator\Rules;

interface RuleInterface
{
    /**
     * Validator fail message
     * @param $attribute
     * @return string
     */
    public function message($attribute): string;

    /**
     * Handle attribute validation
     * @param $value
     * @return bool
     */
    public function validate($value): bool;
}