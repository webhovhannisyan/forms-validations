<?php

namespace Validator\Rules;

class Numeric extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public function message($attribute): string
    {
        return "Attribute {$attribute} must be type of number.";
    }

    /**
     * @inheritDoc
     */
    public function validate($value): bool
    {
        return is_numeric($value);
    }
}