<?php

namespace Form;

abstract class AbstractForm
{
    /**
     * Form attributes
     * @var
     */
    protected $attributes = [];

    /**
     * Initialise attributes
     * @param array|null $attributes
     */
    public function __construct(array $attributes = null)
    {
        $this->attributes = $attributes;
    }

    /**
     * Set form attribute
     * @param $attribute
     * @param $value
     */
    public function setAttribute($attribute, $value): void
    {
        $this->attributes[$attribute] = $value;
    }

    /**
     * Get form attributes
     * @param array $attributes
     * @return void
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * Get form attribute
     * @param $attribute
     * @return mixed|null
     */
    public function getAttribute($attribute)
    {
        return $this->attributes[$attribute] ?? null;
    }

    /**
     * Get form attributes
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
}