<?php

namespace Form;

use Traits\FormValidator;
use Validator\Rules\Email;
use Validator\Rules\InArray;
use Validator\Rules\Integer;
use Validator\Rules\Required;

class LoginForm extends AbstractForm
{
    use FormValidator;

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            'email' => [new Required, new Email],
            'name' => [new Required],
            'gender' => [new InArray(['male', 'female'], false)],
            'age' => [new Required, new Integer]
        ];
    }
}