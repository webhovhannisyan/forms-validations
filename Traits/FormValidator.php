<?php

namespace Traits;

use Exceptions\FormException;
use Exceptions\ValidatorException;
use Form\AbstractForm;
use Validator\Validator;

trait FormValidator
{
    use ErrorsBag;

    /**
     * Validate attributes
     * @return Validator
     * @throws FormException
     * @throws ValidatorException
     */
    public function validate(): Validator
    {
        if (! $this instanceof AbstractForm) {
            throw new FormException('Target class ' . self::class .  ' using ' . FormValidator::class . ' must be instance of ' . AbstractForm::class);
        }
        // Create validator instance and validate
        $validator = (new Validator)->make($this->rules(), $this->getAttributes());
        // Set errors
        $this->addErrors(
            $validator->getErrors()
        );
        // Return instance of validator
        return $validator;
    }

    /**
     * Define rules
     * @return array
     */
    abstract public function rules(): array ;
}